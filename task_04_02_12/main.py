# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_12.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


a = int(input("a = "))
b = int(input("b = "))

for i in range(a, b + 1):
    print(i, end = " ")
print(sep = "n/")
for i in range(b, a - 1, -1):
    print(i, sep = "/n")

# --------------
# Пример вывода:
#
# a = 1
# b = 5
# 1 2 3 4 5
# 5
# 4
# 3
# 2
# 1
