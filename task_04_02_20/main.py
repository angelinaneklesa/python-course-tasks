# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_20.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


n= int(input('n = '))

for i in range(100, 1000):
	i1 = i // 100
	i2 = i // 10 % 10
	i3 = i % 10
	if i1+i2+i3 == n:
		print(i, end=' ')
	else:
		continue

# --------------
# Пример вывода:
#
# n = 3
# 102 111 120 201 210 300
