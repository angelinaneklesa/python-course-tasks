# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_06.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


a = float(input("a = "))
b = float(input("b = "))
c = float(input("c = "))
 
discr = b ** 2 - 4 * a * c

if discr > 0:
    x1 = (-b - (discr**(1/2))) / (2 * a)
    x2 = (-b + (discr**(1/2))) / (2 * a)
    print("x1 = %.1f, x2 = %.1f" % (x1, x2))
elif discr == 0:
    x = -b / (2 * a)
    print("x = %.1f" % x)
else:
    print("Решений нет")

# --------------
# Пример вывода:
#
# a = 1
# b = 2
# c = 3
# Решений нет
#
# a = 1
# b = 2
# c = 1
# x = -1.0
#
# a = 1
# b = 3
# c = -4
# x1 = -4.0, x2 = 1.0
