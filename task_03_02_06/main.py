# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_03_02_06.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!

a = int(input("a="))
b = int(input("b="))
m = int(input("m="))
n = int(input("n="))

x = -b / a

is_ok = m < x < n

print("Попадает:", is_ok)



# --------------
# Пример вывода:
#
# Введите a = 1
# Введите b = 2
# Введите m = -5
# Введите n = 5
# Попадает: True
