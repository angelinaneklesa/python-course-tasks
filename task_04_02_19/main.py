# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_19.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


a = int(input("a="))
b = int(input("b="))
c = int(input("c="))

for a in range(b):
    if((a + 1) % c == 0):
        print(a + 1, end =" ")

# --------------
# Пример вывода:
#
# a = 1
# b = 10
# c = 2
# 2 4 6 8 10
