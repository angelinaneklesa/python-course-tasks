# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_13.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


a = int(input("a = "))
b = int(input("b = "))
n_sum = 0
n_mult = 1 
num = 0
num_1 = 0
n_geom = 1
for i in range(a, b + 1):
    n_sum = n_sum + i
    num += 1
    n_mult = n_mult * i
    n_avg = n_sum / num
    for i in range(a, b + 1):
        if i % 2 == 1:
            num_1 += 1
            n_geom = n_geom * i
    n_avg_geom = n_geom ** (1/num_1)
print("Сумма =", n_sum)
print("Среднее арифметическое = {0:.2f}".format(n_avg))
print("Произведение =", n_mult)
print("Среднее геометрическое нечетных чисел = {0:.2f}".format(n_avg_geom))


# --------------
# Пример вывода:
#
# a = 1
# b = 5
# Сумма = 15
# Произведение = 120
# Среднее арифметическое = 3.00
# Среднее геометрическое нечетных чисел = 2.47
