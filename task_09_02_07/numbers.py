# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_09_02_07.
#
# Выполнил: Фамилия И.О.
# Группа: !!!
# E-mail: !!!


def gcd(first, second):
    """Вернуть НОД для целых чисел 'first' и 'second'.

    Пример:
        - gcd(54, 24) == 6;
        - gcd(-54, 24) == 6.

    Исключения:
        - TypeError: 'first' или 'second' - любой тип кроме int;
        - ValueError: 'first' или second равны 0.
    """
    # Удалите комментарий и допишите код


def lcm(first, second):
    """Вернуть НОК для чисел 'first' и 'second'.

    Пример:
        - lcm(4, 6) == 12;
        - lcm(-4, 6) == 12.

    Исключения:
        - TypeError: 'first' или 'second' - любой тип кроме int;
        - ValueError: 'first' или second равны 0.

    """
    # Удалите комментарий и допишите код


def is_prime(number):
    """Вернуть True, если 'number' - простое число, иначе False.

    Пример: is_prime(7) == True.

    Исключения:
        - TypeError: 'number' - любой тип кроме int;
        - ValueError: 'number' - не натуральное число.
    """
    # Удалите комментарий и допишите код


def inverse(number):
    """Вернуть число, обратное 'number' (1 / number).

    Пример: inverse(2) == 0.5.

    Исключения:
        - TypeError: 'number' - любой тип кроме int, float;
        - ZeroDivisionError: 'number' - 0.
    """
    # Удалите комментарий и допишите код


def root(number, power=2):
    """Вернуть корень 'power'-степени из 'number'.

    Пример: root(9) == 3.0.

    Исключения:
        - TypeError: 'number' - любой тип кроме int, float;
        - TypeError: 'power' - любой тип кроме int, float;
        - ValueError: 'power' - меньше единицы.
    """
    # Удалите комментарий и допишите код
