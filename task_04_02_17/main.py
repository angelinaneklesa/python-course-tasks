# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_17.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


nums_sum = 0
nums_count = 0
i = 1
while True:
    c = int(input("Введиет "+str(i)+"-е число:"))
    i += 1
    nums_sum = nums_sum + c
    if c == 0:
        break
nums_count = i - 2
print(" Сумма:" , nums_sum, "\n" , "Количество:" , nums_count)


# --------------
# Пример вывода:
#
# Введите 1-е число: 1
# Введите 2-е число: 2
# Введите 3-е число: 3
# Введите 4-е число: 4
# Введите 5-е число: 0
# Сумма = 10
# Количество = 4
#
# Введите 1-е число: 0
# Сумма = 0
# Количество = 0
