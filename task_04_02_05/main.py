# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_05.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!

x = int(input("Введите координату х:"))
y = int(input("Введите координату y:"))

if x>0 and y>0:
    print('1-я четверть')
elif x<0 and y>0:
    print('2-я четверть')
elif x<0 and y<0:
    print('3-я четверть')
elif x>0 and y<0:
    print('4-я четверть')

