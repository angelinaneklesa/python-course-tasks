# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_04.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


year_today = int(input("Введите текущий год:"))
month_today = int(input("Введите текущий месяц:"))

year = int(input("Введите год рождения:"))
month = int(input("Введите месяц рождения:"))

print("Число полных лет:",(((year_today - year) * 12 + month_today - month) // 12))
