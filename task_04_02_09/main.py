# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_09.
# Выполнил: Неклеса А.А.
# Группа: Ис
# E-mail: !!!

a = float(input("Введите а: "))
x_sum, n = 0.0, 0
while x_sum <= a:
    n += 1
    x_sum += 1/n
print("n=",n)

