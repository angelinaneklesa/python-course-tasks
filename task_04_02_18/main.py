# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_18.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


s = input("Введите строку:")
count_gl = 0
count_cogl = 0
vowels = set("уеаоэяиюУЕЫАОЯИЮ")
consonants = set('йЙцЦкКнНгГшШщЩзЗхХъЪфФыЫвВпПрРлЛдДЖжчЧсСмМтТьЬбБ')
for letter in s:
	if letter in vowels:
		count_gl += 1
		vowels = set("уеаоэяиюУЕЫАОЯИЮ")

for letter in s:
	if letter in consonants:
		count_cogl += 1
		continue
print("Кол-во букв в предложении: гласных -", count_gl, ", согласных - ", count_cogl)

# --------------
# Пример вывода:
#
# Введите предложение: Программирование
# Кол-во букв в предложении: гласных - 7, согласных - 9
