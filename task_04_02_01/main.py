# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_01.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


x =float(input("x="))

if x >= 0:
    f=x**(1/2)+x**2
else:
    f = 1 / x

print(f)

