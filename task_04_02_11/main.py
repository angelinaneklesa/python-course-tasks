# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_11.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!

start = int(input("start="))
k = int(input("k="))
s = int(input("s="))
n = 0
while n < 10:
    if start % 10 == k and start % s == 0:
        n += 1
        print(start, end=' ')
    start = start + s if n != 0 else start + 1
