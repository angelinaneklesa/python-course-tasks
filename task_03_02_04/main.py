# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_03_02_04.
#
# Выполнил: Неклеса А.А.
# Группа: 2 курс ИС
# E-mail: !!!


# Двузначное число
num2 = int(input("num2="))
# Трехзначное число
num3 = int(input("num3="))

# 1-я цифра числа 'num2'
num2_1 = (num2 % 10)
# 2-я цифра числа 'num2'
num2_2 = (num2 // 10)

# Сумма цифр числа 'num2'
num2_s = print(num2_1 + num2_2)
# Произведение цифр числа 'num2'
num2_p = print(num2_1 * num2_2)

# 1-я цифра числа 'num3'
num3_1 = (num3 % 10)
# 2-я цифра числа 'num3'
num3_2 = (num3 % 100 // 10)
# 3-я цифра числа 'num3'
num3_3 = (num3 // 100)

# Сумма цифр числа 'num3'
num3_s = print(num3_1 + num3_2 + num3_3)
# Произведение цифр числа 'num3'
num3_p = print(num3_1 * num3_2 * num3_3)
