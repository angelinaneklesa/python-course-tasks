# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_10.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!

a =int(input("a="))
def s(a):
    result = 0
    while a > 0:
        result += a % 10
        a //= 10
    return result
 
print("Сумма =", s(a))
print("Количество =", len(str(a)))
