# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_08.
#
# Выполнил: еклеса А.А.
# Группа: ИС
# E-mail: !!!


n = int(input("n= "))
flag = 0
while flag <= n:
    print(flag)
    flag += 5

# --------------
# Пример вывода:
#
# n = 0
# 0
#
# n = 10
# 0
# 5
# 10
#
# n = 12
# 0
# 5
# 10
