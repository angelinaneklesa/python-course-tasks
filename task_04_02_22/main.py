# Программирование на языке высокого уровня (Python).
# https://www.yuripetrov.ru/edu/python
# Задание task_04_02_22.
#
# Выполнил: Неклеса А.А.
# Группа: ИС
# E-mail: !!!


n = int(input("n="))
b= []
a_max = None
a_min = None
i = 1
while i <= n:
    c = float(input(str(i)+"-е число:"))
    i += 1
    b.append(c)
a_max = max(b)
a_min = min(b)



print("Максимум: {:.2f}".format(a_max))
print("Минимум: {:.2f}".format(a_min))

# --------------
# Пример вывода:
#
# n = 4
# 1-е число = 6.2
# 2-е число = 3.8
# 3-е число = 1.1
# 4-е число = 9.66
# Максимум: 9.66
# Минимум: 1.10
